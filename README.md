NYL-Export-Policy-Role
=========

This role utilizes the NetApp API to create export policies. 
The latest documentaion on the Ansible/NetApp API used by this role can be found at: 
https://docs.ansible.com/ansible/2.9_ja/modules/na_ontap_export_policy_module.html

Requirements
------------
This role requires:

1. Python, Version 3.6.9
2. netapp.ontap, Version 21.10.0
3. netapp-lib, Version 2021.6.25

Role Variables
--------------
(Required)

*netapp_hostname:*

   - This variable should be set to the NetApp hostname that is used to manage the export policy
   
*netapp_username:*

   - This variable should be set to the NetApp username that is used to manage the export policy
   
*netapp_password:*

   - This variable should be set to the NetApp password that is used to manage the export policy

*netapp_svm:*

   - This variable will set the VServer that will be used to manage the export policy

*export_policy_name:*
   
   - This variable will be used to name the export policy

(Optional)

*protocol_http_port:*

   - Override the default port (80 or 443) with this port
   
*protocol_from_name:*

   - The name of the export-policy to be renamed.
   
*protocol_ontapi:*

   - The ontap api version to use

*protocol_use_rest:*

   - REST API if supported by the target system for all the resources and attributes the module requires. Otherwise will revert to ZAPI.

*protocol_validate_certs:*
   
   - If set to no, the SSL certificates will not be validated. This should only set to False used on personally controlled sites using self-signed certificates.

Example Playbook
----------------

    ---
    - name: Create Export Policy
      hosts:
        - all
      gather_facts: false
      become: false
      vars_files:
        - vars/ontap_creds.yml
        - vars/main.yml
      roles:
        - nyl-export-policy-role



Author Information
------------------

This role was developed by Enterprise Vision Technologies (*www.evtcorp.com*)
